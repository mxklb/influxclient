#!/usr/bin/env python
# A simple test for the influxclient.py
import time
import argparse
import influxclient
from datetime import datetime

# Define influxdb setup
influxurl = "demonstrator1"
influxport = 8086
influxuser = "influx-writer"
influxpass = "password"
database = "showcase"

interval = 10

# Configure argument parser ..
parser = argparse.ArgumentParser(description="influxtest.py")
parser.add_argument('--influxurl', type=str, default=influxurl, nargs='?', help='The url of the influxdb')
parser.add_argument('--influxport', type=int, default=influxport, nargs='?', help='The port of the influxdb')
parser.add_argument('--influxuser', type=str, default=influxuser, nargs='?', help='The user of the influxdb')
parser.add_argument('--influxpass', type=str, default=influxpass, nargs='?', help='The password for the given user')
parser.add_argument('--database', type=str, default=database, nargs='?', help='The database to write into')
args = parser.parse_args()

influxurl = args.influxurl
influxport = args.influxport
influxuser = args.influxuser
influxpass = args.influxpass
database = args.database
#print(influxurl, influxport, influxuser, influxpass, database)

# Create static data for influx write testing ..
tags = {}
tags["tag1"] = "incredible"
tags["tag2"] = "wonderful"
fields = {}
fields["field1"] = 1234.56789
fields["field2"] = 0.01234567
dict = {}
dict["measurement"] = "test_measurement"
dict["tags"] = tags
dict["time"] = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S.%f")
dict["fields"] = fields

# Setup influx client ..
influxclient.setup(influxurl, influxport, influxuser, influxpass, database)

# Start main loop
while True:
    dict["time"] = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S.%f")
    # Write json into influxdb
    if influxclient.try_to_write_json([dict]):
        print("Successfully wrote into influxdb: " + str(dict))
    time.sleep(interval)
