# Influx Client
A simple wrapper around [influxdb python](https://github.com/influxdata/influxdb-python)
to easily write into an influxdb.

## Setup Dependencies
    pip3 install influxdb

## How to use this client
    import influxclient
    influxclient.setup(influxurl, influxport, influxuser, influxpass, database)
    influxclient.try_to_write_json([dict])

Note: `[dict]` must be influxdb conform dictionaries. The array of measurement points
is written using [`write_points()`](https://github.com/influxdata/influxdb-python#examples).

A single measurement point may looks like:

    {
      "measurement": "test_measurement",
      "tags": {
        "tag1": "incredible",
        "tag2": "wonderful"
      },
      "time": "2020-03-28 13:00:21.492754",
      "fields": {
        "field1": 1234.56789,
        "field2": 0.01234567
      }
    }
