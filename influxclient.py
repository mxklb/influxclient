#!/usr/bin/env python
# This is a wrapper around InfluxDBClient to easily write into influxdb.
# Usage: See influxtest.py, import it and then call ..
# influxclient.setup(influxurl, influxport, influxuser, influxpass, database)
# influxclient.try_to_write_json([dict])
# Note: [dict] is an influx conform array of datapoints to be written!
from influxdb import InfluxDBClient
from datetime import datetime
import sys

influxsetup = []
client = None

# Helper: UTC time converter
class to_utc():
    utc_delta = datetime.utcnow() - datetime.now()
    def __call__(cls, t):
        return t + cls.utc_delta

# Initialize and get influxdb client ..
def setup(influxurl, influxport, influxuser, influxpass, database):
    global influxsetup, client
    influxsetup = [influxurl, influxport, influxuser, influxpass, database]
    client = InfluxDBClient(influxurl, influxport, influxuser, influxpass, database)

# Close and reconnect client ..
def reconnect_influxdb_client():
    try:
        global influxsetup, client
        client.close()
        #time.sleep(0.01)
        if len(influxsetup) != 0:
            client = InfluxDBClient(influxsetup[0], influxsetup[1], influxsetup[2], influxsetup[3], influxsetup[4])
    except: # catch *all* exceptions
        e = sys.exc_info()[0]
        print("Exception: %s" % e)

# Write given json into influxdb ..
def write_json(json, precision='u'):
    try:
        global client
        if client.write_points(json, time_precision=precision):
            return True
        return False
    except: # catch *all* exceptions
        e = sys.exc_info()[0]
        print("Exception: %s" % e)
        return False

# Write json to influxdb. If fails, reconnect client & retry
def try_to_write_json(json, retry_count=3, precision='u'):
    retrys = 0
    success = write_json(json, precision)
    while not success and retrys < retry_count:
        reconnect_influxdb_client()
        success = write_json(json, precision)
        retrys += 1
    if not success:
        return False
    return True
